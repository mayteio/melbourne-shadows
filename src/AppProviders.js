import React from "react";

import { ThemeProvider } from "@material-ui/styles";
import { ThemeProvider as StyledThemeProvider } from "styled-components";
import { lightTheme } from "./common/theme";

import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { MapProvider } from "./common/MapContext";

export default function AppProviders({ children }) {
  return (
    <ThemeProvider theme={lightTheme}>
      <StyledThemeProvider theme={lightTheme}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <MapProvider>{children}</MapProvider>
        </MuiPickersUtilsProvider>
      </StyledThemeProvider>
    </ThemeProvider>
  );
}
