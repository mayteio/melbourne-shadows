import React from "react";
import { useViewstate, useMapRef } from "./MapContext";
import DeckGL from "@deck.gl/react";
import { StaticMap } from "react-map-gl";
import { MapboxLayer } from "@deck.gl/mapbox";

export default function Map({ layers, lighting, mapStyle, children }) {
  const [viewstate, setViewstate] = useViewstate();

  const [gl, setGl] = React.useState();

  const mapRef = useMapRef();
  const deckRef = React.useRef();

  const onLoad = e =>
    layers.forEach(layer =>
      mapRef.current.addLayer(
        new MapboxLayer({ id: layer.id, deck: deckRef.current }),
        "place-suburb"
      )
    );
  return (
    <DeckGL
      layers={layers}
      effects={[lighting]}
      controller={{ touchRotate: true }}
      viewState={viewstate}
      onWebGLInitialized={gl => gl && setGl(gl)}
      onViewStateChange={v => setViewstate(v.viewState)}
      ref={ref => (deckRef.current = ref && ref.deck)}
    >
      <StaticMap
        reuseMaps
        mapStyle={mapStyle}
        mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_API_ACCESS_TOKEN}
        gl={gl}
        onLoad={onLoad}
        ref={ref => (mapRef.current = ref && ref.getMap())}
      >
        {children}
      </StaticMap>
    </DeckGL>
  );
}
