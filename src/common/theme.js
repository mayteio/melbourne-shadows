import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import "./fonts/fonts.css";

const fontFamily = [
  "Gotham",
  "Roboto",
  "-apple-system",
  "BlinkMacSystemFont",
  '"Segoe UI"',
  '"Helvetica Neue"',
  "Arial",
  "sans-serif",
  '"Apple Color Emoji"',
  '"Segoe UI Emoji"',
  '"Segoe UI Symbol"'
].join(",");

const headingFontFamily = `"CoM", ${fontFamily}`;
const bodyFontFamily = `Gotham, ${fontFamily}`;

/**
 * Primary colour palette is dark background.
 * Secondary is white background.
 */

export const lightTheme = createMuiTheme({
  palette: {
    primary: { main: "#e50e56" },
    secondary: { main: "#277bb4" },
    error: { main: "#F07B05" },
    grey: {
      100: "#f2f3f4",
      200: "#d4d6db",
      300: "#888B93",
      400: "#3c404b",
      500: "#30323B",
      600: "#23242b",
      700: "#16161A",
      800: "#080809"
    }
  },
  typography: {
    // Use the system font instead of the default Roboto font.
    fontFamily,
    h1: { fontFamily: headingFontFamily },
    h2: { fontFamily: headingFontFamily },
    h3: { fontFamily: headingFontFamily },
    h4: { fontFamily: headingFontFamily },
    h5: { fontFamily: bodyFontFamily },
    h6: { fontFamily: bodyFontFamily, fontSize: 30 }
  },
  overrides: {
    MuiListItemText: {
      primary: {
        // fontWeight: "bold"
      }
    },
    MuiTab: {
      wrapper: {
        // color: "#fff"
      }
    }
  }
});

export const darkTheme = createMuiTheme({
  palette: {
    primary: { main: "#e50e56" },
    secondary: { main: "#277bb4" },
    error: { main: "#F07B05" },
    grey: {
      100: "#f2f3f4",
      200: "#d4d6db",
      400: "#3c404b",
      600: "#23242b",
      800: "#080809"
    }
  },
  typography: {
    // Use the system font instead of the default Roboto font.
    fontFamily,
    h1: { fontFamily: headingFontFamily },
    h2: { fontFamily: headingFontFamily },
    h3: { fontFamily: headingFontFamily },
    h4: { fontFamily: headingFontFamily },
    h5: { fontFamily: bodyFontFamily },
    h6: { fontFamily: bodyFontFamily }
  },
  overrides: {}
});
