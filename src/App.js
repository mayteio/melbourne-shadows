import React from "react";

import suncalc from "suncalc";

import { Box } from "@material-ui/core";

import { generateLighting } from "./lightingEffects";
import { generateLayers } from "./layers";

import { useMapRef, DEFAULT_VIEW_STATE } from "./common/MapContext";
import Map from "./common/Map";
import Header from "./common/Header";
import Branding from "./common/Branding";

import Controls from "./Controls";

const day = "mapbox://styles/gisfeedback/ck065yh57135w1crqs3gp4g3g?fresh=true";

function App() {
  const [lighting, setLighting] = React.useState(generateLighting(new Date()));
  const [timeOfDay, setTimeOfDay] = React.useState("day");

  const mapRef = useMapRef();

  const onChange = React.useCallback(
    ({ time, date }) => {
      date.setHours(time);
      setLighting(generateLighting(date));

      const { sunrise, sunset } = suncalc.getTimes(
        date,
        DEFAULT_VIEW_STATE.latitude,
        DEFAULT_VIEW_STATE.longitude
      );

      const nextTimeOfDay = date > sunrise && date < sunset ? "day" : "night";
      if (timeOfDay !== nextTimeOfDay) {
        setTimeOfDay(nextTimeOfDay);
        setMapStyle(mapRef.current, nextTimeOfDay);
      }
    },
    [mapRef, timeOfDay]
  );

  const layers = generateLayers(timeOfDay);

  return (
    <Box display="flex" flexDirection="column" height={1}>
      <Header>
        <Branding />
      </Header>
      <Box position="relative" flexGrow={1}>
        <Controls onChange={onChange} />
        <Map layers={layers} lighting={lighting} mapStyle={day} />
      </Box>
    </Box>
  );
}

export default App;

function setMapStyle(map, theme) {
  if (!map) return;

  const colors =
    theme === "night"
      ? {
          roads: "#1e2e38",
          water: "#112330",
          parks: "#09101d",
          background: "#09101d"
        }
      : {
          roads: "#fff",
          water: "#cce3f0",
          parks: "#d7ead9",
          background: "#ebf0f5"
        };
  const style = map.getStyle();
  style.layers.forEach(layer => {
    if (
      ["line", "fill"].includes(layer.type) &&
      layer.id.match(/(bridge|road|tunnel)/g) !== null
    ) {
      layer.paint[`${layer.type}-color`] = colors.roads;
    }

    if (
      ["line", "fill"].includes(layer.type) &&
      layer.id.match(/(water)/g) !== null
    ) {
      layer.paint[`${layer.type}-color`] = colors.water;
    }

    if (
      ["line", "fill"].includes(layer.type) &&
      layer.id.match(/(park)/g) !== null
    ) {
      layer.paint[`${layer.type}-color`] = colors.parks;
    }

    if (layer.id === "background") {
      layer.paint["background-color"] = colors.background;
    }
  });

  map.setStyle(style);
}
