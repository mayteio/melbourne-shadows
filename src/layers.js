import { PolygonLayer, GeoJsonLayer } from "@deck.gl/layers";
import { material } from "./lightingEffects";

const landCover = [
  [
    [144.89147186279297, -37.86726491715301],
    [144.99584197998044, -37.86726491715301],
    [144.99584197998044, -37.77478541213122],
    [144.89147186279297, -37.77478541213122],
    [144.89147186279297, -37.86726491715301]
  ]
];

export const generateLayers = timeOfDay => [
  // This is only needed when using shadow effects
  new PolygonLayer({
    id: "ground",
    data: landCover,
    getPolygon: f => f,
    stroked: false,
    getFillColor: [0, 0, 0, 0]
  }),
  new GeoJsonLayer({
    id: "buildings",
    data:
      "https://city-dna.s3-ap-southeast-2.amazonaws.com/building_footprints.json",
    extruded: true,
    wireframe: false,
    opacity: 1,
    getElevation: f => f.properties.footprint_extrusion,
    getFillColor: timeOfDay === "day" ? [255, 255, 255] : [74, 80, 87],
    material: material
  })
];
